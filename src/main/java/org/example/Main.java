package org.example;

import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<String, String> allItems = PosDataLoader.loadAllItems();
        String[] cart = PosDataLoader.loadCart();
        String[] promotions = PosDataLoader.loadPromotion();

        generateReceiptByBarcodes receipt = new generateReceiptByBarcodes();

        String receiptOutput = receipt.calculateReceipt(allItems, cart, promotions);
        System.out.println(receiptOutput);
    }
}