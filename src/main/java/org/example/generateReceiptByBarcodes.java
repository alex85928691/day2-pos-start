package org.example;
import java.util.*;

public class generateReceiptByBarcodes {

    public String calculateReceipt(Map<String, String> allItems, String[] cart, String[] promotions) {
        List<Item> purchased = processPurchases(allItems,cart);
        double purchasedTotal = calculateTotal(purchased);

        Map<String, Item> promoItems = calculateSaved(allItems, purchased, promotions);
        double freeTotal = calculateTotal(new ArrayList<>(promoItems.values()));

        return prepareReceipt(purchased, promoItems, purchasedTotal, freeTotal);
    }

    private List<Item> processPurchases(Map<String, String> allItems, String[] cart) {
        Map<String, Item> itemsMap = new HashMap<>();
        for (String itemCode : cart){
            String[] parts = itemCode.split("-");
            String code = parts[0];
            double quantity = parts.length > 1? Double.parseDouble(parts[1]) : 1;


            Item item = getItem(allItems,code,itemsMap);
            item.increaseQuantity(quantity);
        }

        return new ArrayList<>(itemsMap.values());
    }

    private Item getItem(Map<String, String> allItems, String code, Map<String, Item> itemsMap) {
        if (!itemsMap.containsKey(code)) {
            String[] itemProperties = allItems.get(code).split(",");
            itemsMap.put(code, new Item(code, itemProperties[0], Double.parseDouble(itemProperties[1]), itemProperties[2]));
        }
        return itemsMap.get(code);
    }

    private double calculateTotal(List<Item> purchasedItems) {
        double price= 0.0;
        for(Item item: purchasedItems){
            price += item.getSubTotal();
        }
        return price;
    }

    private Map<String, Item> calculateSaved(Map<String, String> allItems, List<Item> purchasedItems, String[] promotions) {
        List<String> promoList = Arrays.asList(promotions);
        Map<String,Item> promoItems = new HashMap<>();

        for (Item item: purchasedItems){
            if(promoList.contains(item.barcode) && item.quantity >3){
                Item promoItem = getItemForPromo(allItems,item);
                promoItems.put(item.barcode,promoItem);
            }
        }
        return promoItems;
    }

    private Item getItemForPromo(Map<String, String> allItems, Item item) {
            Item promo = new Item(item.barcode,item.name,item.price,item.unit);
            promo.increaseQuantity((int)(item.quantity/3));

        return promo;
    }

    private String prepareReceipt(List<Item> purchasedItems, Map<String, Item> promoItems, double total, double saved) {
        StringBuilder receipt = new StringBuilder("***<No Profit Store> Receipt***\n\n----------------------\n");
        for (Item item : purchasedItems) {
            receipt.append(formatItemString(item));
        }
        receipt.append("----------------------\nBuy two get one free items：\n");
        for (Item item : promoItems.values()) {
            receipt.append(formatPromoItemString(item));
        }
        receipt.append("----------------------\n")
                .append(String.format("Total: %.2f (CNY)\nSaved: %.2f (CNY)\n", total, saved))
                .append("**********************\n");
        return receipt.toString();
    }

    private String formatItemString(Item item) {
        return String.format("Name: %s, Quantity: %.2f %s, Unit Price: %.2f (CNY), Subtotal: %.2f (CNY)\n",
                item.name, item.quantity, item.unit, item.price, item.getSubTotal());
    }

    private String formatPromoItemString(Item item) {
        return String.format("Name: %s, Quantity: %.0f %s, Value: %.2f (CNY)\n",
                item.name, item.quantity, item.unit, item.getSubTotal());
    }

    class Item {
        String barcode;
        String name;
        double price;
        String unit;
        double quantity;

        public Item(String barcode, String name, double price, String unit) {
                this.barcode = barcode;
                this.name = name;
                this.price = price;
                this.unit = unit;
                this.quantity = 0;
        }

        public void increaseQuantity(double quantity) {
            this.quantity += quantity;
        }

        public double getSubTotal() {

            return price*quantity;
        }
    }
}